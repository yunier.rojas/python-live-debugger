# python-live-debugger
# Copyright (C) 2022 Yunier Rojas García
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
Utility to transform a stack frame into a dataclass without mutating any data. Each time a patched code executes,
:py:func:`transform_frame` receives the current stack frame and transform it into a list of
:py:class:`live_debugger.models.FrameData`. For performance reasons this module is compiled to C using Cython.
"""

__all__ = ["transform_frame"]

import functools
import inspect
import io
import types
import typing
from collections import abc

from live_debugger import models


def _next(obj, depth):
    return _transform_object(obj, depth - 1) if depth else repr(obj)


@functools.singledispatch
def _transform_object(obj, depth=2) -> typing.Any:
    """
    Transform objects into more safe types that can be passed to callback function for processing. Given that some
    objects can be recursive or contain long concatenation of other objects, the function only reach certain depth.

    Args:
        obj: Object being transformed
        depth: Remaining depth the function can reach

    Returns: A more safe representation of the object useful enough for debugging purpose.

    """

    return repr(obj)


@_transform_object.register(str)
@_transform_object.register(int)
@_transform_object.register(float)
@_transform_object.register(complex)
@_transform_object.register(bytes)
def _(obj, _) -> typing.Any:
    return obj


@_transform_object.register
def _(obj: types.ModuleType, _) -> typing.Any:
    return {
        "file": obj.__file__ if hasattr(obj, __file__) else None,
        "name": obj.__name__,
        "package": obj.__package__,
    }


@_transform_object.register
def _(obj: types.GeneratorType, depth: int = 2) -> typing.Any:
    return {
        "name": obj.__name__,
        "qualname": obj.__qualname__,
        "is_running": obj.gi_running,
        "frame": _next(obj.gi_frame, depth),
    }


@_transform_object.register(abc.Iterator)
@_transform_object.register(io.IOBase)
@_transform_object.register(range)
def _(obj, _) -> typing.Any:
    return repr(obj)


@_transform_object.register
def _(obj: types.FunctionType, depth: int = 2) -> typing.Any:
    return {
        "name": obj.__name__,
        "qualname": obj.__qualname__,
        "module": obj.__module__,
        **_next(obj.__code__, depth),
    }


@_transform_object.register
def _(obj: dict, depth: int = 2) -> typing.Any:
    try:
        return {_next(key, depth): _next(obj[key], depth) for key in obj}
    except TypeError:
        pass


@_transform_object.register(list)
@_transform_object.register(tuple)
@_transform_object.register(set)
def _(obj, depth: int = 2) -> typing.Any:
    try:
        return tuple(_next(item, depth) for item in obj)
    except TypeError:
        pass


def transform_frame(frame: types.FrameType, depth=2) -> typing.List[models.FrameData]:
    """
    Transform a stack frame into a more safe data type that can be passed to callback function for processing.
    Given that some objects can be recursive or contain long concatenation of other objects, the function only
    reach certain depth.

    Args:
        frame: Stack frame being transformed
        depth: Remaining depth the function can reach

    Returns: A safe data type representation of the stack frame useful enough for debugging purpose.

    """
    return [
        models.FrameData(
            code=_transform_code(frame.f_code),
            locals=_transform_object(frame.f_locals, depth),
        )
    ] + [
        models.FrameData(
            code=_transform_code(info.frame.f_code),
            locals=_transform_object(info.frame.f_locals, depth),
        )
        for info in inspect.getouterframes(frame)
    ]


def _transform_code(obj: types.CodeType) -> models.CodeData:
    """
    Transform a code object into a more safe data type that can be passed to callback function for processing.

    Args:
        obj: Stack frame being transformed

    Returns: A safe data type representation of the code object useful enough for debugging purpose.

    """
    return models.CodeData(
        filename=obj.co_filename,
        flags=obj.co_flags,
        name=obj.co_name,
        first_line_no=obj.co_firstlineno,
    )
