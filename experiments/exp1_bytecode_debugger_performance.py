import sys
from os import path

import pytest

sys.path.insert(0, path.dirname(path.dirname(__file__)))


def for_function():
    for c in range(10, 0, -1):
        if c == 1:
            return c


def nothing(data):
    pass


@pytest.fixture
def bytecode_debugger():
    from bytecode_debugger import debugger

    obj = debugger.LiveDebugger()
    yield obj
    obj.deactivate()


@pytest.fixture
def tracing_debugger():
    from live_debugger import debugger

    obj = debugger.LiveDebugger()
    yield obj
    obj.deactivate()


def test_tracing_debugger_with_callback_should_be_fast(tracing_debugger, benchmark):
    tracing_debugger.activate()
    tracing_debugger.add_point("exp1_debuggers_performance.py", 11, nothing)
    benchmark(for_function)


def test_bytecode_debugger_with_callback_should_be_fast(bytecode_debugger, benchmark):
    bytecode_debugger.activate()
    bytecode_debugger.add_point("exp1_debuggers_performance.py", 11, nothing)
    benchmark(for_function)
