# python-live-debugger
# Copyright (C) 2022 Yunier Rojas García
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
__all__ = ["LiveDebugger"]

import collections
import types
import typing
import uuid

from live_debugger import data
from live_debugger import typing as ld_typing

from . import _bytecode, _code

_KeyType = typing.Tuple[int, int]
_PointDataType = typing.DefaultDict[
    _KeyType,
    typing.Dict[str, typing.Tuple[ld_typing.CallbackType, ld_typing.ConditionType]],
]


class LiveDebugger(ld_typing.LiveDebuggerProtocol):
    """
    Registry to keep record of installed points in each code object.

    This class keeps a record of all patches lines of a code object with all callbacks and conditions. Each time a
    point is added using :py:meth:`PointRegistry.add_point` stores all necessary information to trigger the callback
    when the code is executed or to clear the point using :py:meth:`PointRegistry.clear_point`.

    The module is compiled to C using `Cython <https://cython.org/>`_ to provide fast code execution when needed the
    most: when the patched code kicks in, the frame stack needs to be "exported" to a more friendly format and be
    passed to all callbacks matching the condition.

    """

    __slots__ = ["_callbacks", "_markers", "_cookies", "_codes", "_active"]

    def __init__(self):
        self._callbacks = collections.defaultdict(dict)  # type: _PointDataType
        self._markers = {}  # type: typing.Dict[_KeyType, ld_typing.CookieType]
        self._codes = {}  # type: typing.Dict[_KeyType, types.CodeType]
        self._cookies = {}  # type: typing.Dict[ld_typing.CookieType, _KeyType]
        self._active = False

    def _make_safe_handler(
        self, key: _KeyType
    ) -> typing.Callable[[types.FrameType], None]:
        """
        Function factory that takes a tuple if (code id, line number) and returns a function capable of handling
        the frame object from the code point. The built function transform :py:class:`types.FrameType` into a
        :py:class:`live_debugger.models.FrameData` object and passed into all callbacks of the key which the condition
        activates.

        Args:
            key: Tuple of code id and line number

        Returns: Function to handle a stack frame

        """

        def handler(frame: types.FrameType):
            info = data.transform_frame(frame)

            for cb, cond in self._callbacks.get(key, {}).values():
                if cond is None or eval(  # nosec B307
                    cond, frame.f_globals, frame.f_locals
                ):
                    cb(info)

        return handler

    def add_point(
        self,
        fn: str,
        line: int,
        callback: ld_typing.CallbackType,
        *,
        condition: ld_typing.ConditionType = None
    ) -> ld_typing.CookieType:
        """
        Stores a callback and a condition for activate it when the code runs. If the specified code does not have a
        bytecode patch already installed, this method will install it but only once.

        Args:
            fn: Filename hosting the source code
            line: Line number
            callback: Function handling the data processed from the stack frame
            condition: Condition to decide whether to call the callback or not

        Returns: Unique cookie that identifies all changes made to the code with the data stored

        """
        code_obj = _code.get_code(fn, line)
        key = (id(code_obj), line)
        cookie = str(uuid.uuid4())

        self._cookies[cookie] = key
        self._codes[key] = code_obj
        self._callbacks[key][cookie] = (callback, condition)

        self._activate_key(key)

        return cookie

    def clear_point(self, cookie: str):
        """
        Clear all data associated with a cookie.

        Args:
            cookie: Unique cookie identifying the previous installed callback.

        """
        key = self._cookies[cookie]
        del self._callbacks[key][cookie]

        if not self._callbacks[key]:
            self._deactivate_key(key)
            del self._callbacks[key]

        del self._cookies[cookie]

    def _activate_key(self, key):
        if self._active and key not in self._markers:
            _, line = key
            code_obj = self._codes[key]
            handler = self._make_safe_handler(key)
            marker = _bytecode.patch_code(code_obj, line, handler)
            self._markers[key] = marker

    def _deactivate_key(self, key):
        try:
            code_obj = self._codes[key]
            marker = self._markers[key]
            _bytecode.unpatch_code(code_obj, marker)
            del self._markers[key]
        except KeyError:
            pass

    def is_active(self):
        return self._active

    def activate(self):
        if self.is_active():
            return

        self._active = True
        for key in self._callbacks:
            self._activate_key(key)

    def deactivate(self):
        if not self.is_active():
            return

        for key in self._callbacks:
            self._deactivate_key(key)

        self._active = False
