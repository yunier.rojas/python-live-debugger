# python-live-debugger
# Copyright (C) 2022 Yunier Rojas García
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
Basics
======

Bytecode manipulation helpers that safely manipulate functions bytecode and other attributes of code objects.

`Code objects <https://docs.python.org/3/c-api/code.html>`_
***********************************************************

From Python docs:

   Code objects are a low-level detail of the CPython implementation. Each one
   represents a chunk of executable code that hasn't yet been bound into a function.

These objects are considered internal objects which explains why they are immutable.

`Bytecode <https://docs.python.org/3/library/dis.html>`_
********************************************************

From Python docs:

    Python source code is compiled into bytecode, the internal representation of a Python
    program in the CPython interpreter. The bytecode is also cached in .pyc files so that
    executing the same file is faster the second time (recompilation from source to bytecode
    can be avoided). This “intermediate language” is said to run on a virtual machine that
    executes the machine code corresponding to each bytecode. Do note that bytecodes are not
    expected to work between different Python virtual machines, nor to be stable between Python
    releases.

Internal representation of this data is a
`16-bit wordcode <https://docs.python.org/3/whatsnew/3.6.html#cpython-bytecode-changes>`_.
:math:`2*n` and :math:`2*n+1` indexes of the list represents the nth instruction and its argument respectively.

Line table
**********

Depending on the CPython version, there are different sources for matching bytecode indexes with
line numbers on a source file. Below version `3.9`, attribute `co_lnotab` in the code object is
taken as the source of truth for mapping. On version `3.10` and beyond, the mapping is stored in
attribute `linetable`. Internally these mapping is store as bytes but each attribute has its own
structure. In the case of `linetable`, the structure changes between versions.

Public
======
"""

__all__ = ["patch_code", "unpatch_code"]

import ctypes
import dis
import sys
import types
import typing
import uuid
from sys import _getframe

from . import _code, _native

_PY_VERSION = tuple(sys.version_info[:2])
_FrameHandlerType = typing.Callable[[types.FrameType], None]
_PatchBytecodeDataType = typing.Tuple[bytes, typing.Tuple[typing.Any, ...]]
_PatchBytecodeFactory = typing.Callable[
    [int, typing.Optional[_FrameHandlerType]], _PatchBytecodeDataType
]


def _gen_lnotab_offset(lnotab: bytes, patch_start: int, patch_size: int) -> bytes:
    """
    Generate line number mapping compatible with `co_lnotab` attribute of code object. It requires the bytecode
    index in which the patch was installed and the size of the patch bytecode. If `patch_size` has a negative value,
    it means the bytecode patch was removed.

    Args:
        lnotab: Line number mapping `co_lnotab` attribute
        patch_start: Index of the patch inside the bytecode
        patch_size: Size of the patch in the bytecode. It allows to be negative when the code was removed.

    Returns: `co_lnotab` compatible new version of the line mapping.

    """
    lnotab_list = list(lnotab)
    idx = 0
    limit = len(lnotab)
    s = 0

    while idx < limit:
        s += lnotab_list[idx]
        if s > patch_start:
            lnotab_list[idx] += patch_size
            break
        idx += 2

    return bytes(lnotab_list)


def _gen_linetable_offset(
    linetable: bytes, line_map_idx: int, patch_size: int
) -> bytes:
    """

    Generate line number mapping compatible with `linetable` attribute of code object. It requires the index
    of the updated line in `linetable` and the size of the patch bytecode. If `patch_size` has a negative value,
    it means the bytecode patch was removed.

    Args:
        linetable: Line number mapping `linetable` attribute
        line_map_idx: Index of the changed line in the line table mapping.
        patch_size: Size of the patch in the bytecode. It allows to be negative when the code was removed.

    Returns: `linetable` compatible new version of the line mapping.

    """
    # `linetable` attribute value must be interpreted as a C int8 number
    # because some negative values have special meanings
    linetable_list = [ctypes.c_int8(byte).value for byte in linetable]  # type: ignore[attr-defined]

    if patch_size > 0:
        # `linetable` allows to add mapping entries that do not increase line numbers
        op_delta, line_delta = linetable_list[line_map_idx : line_map_idx + 2]
        new_map = [patch_size, line_delta, op_delta, 0]
        linetable_list[line_map_idx : line_map_idx + 2] = new_map
    else:
        p_size, line_delta, op_delta, padding = linetable_list[
            line_map_idx : line_map_idx + 4
        ]
        if abs(patch_size) != p_size:
            raise ValueError(
                f"Patch size in bytecode ({p_size}) does not match the one requested ({abs(patch_size)})"
            )
        new_map = [op_delta, line_delta]
        linetable_list[line_map_idx : line_map_idx + 4] = new_map

    return b"".join(
        [item.to_bytes(1, byteorder="big", signed=True) for item in linetable_list]
    )


def _get_bytecode_linetable_idx(code_object, bytecode_idx):
    """
    Computes the index in `linetable` of a bytecode index.

    Args:
        code_object: Code object
        bytecode_idx: Index in the bytecode

    Returns: Index in `linetable` where given bytecode starts

    """
    for op, ln, idx in _code._iter_lines_linetable(code_object):
        if op == bytecode_idx:
            return idx


def _bytecode_instr_abs_arg_to_idx(
    arg: int,
) -> int:
    """
    Transform a bytecode instruction argument for absolute jump into a bytecode index.

    Args:
        arg: Bytecode instruction absolute jump argument.

    Returns: Bytecode index offset

    """
    return arg * (2 if _PY_VERSION >= (3, 10) else 1)


def _bytecode_instr_rel_arg_to_idx(
    arg: int,
) -> int:
    """
    Transform a bytecode instruction offset into an index offset. This is necessary because the size
    of bytecode offsets changed in Python 3.10 from one byte to two bytes.

    Args:
        arg: Bytecode instruction offset argument.

    Returns: Bytecode index offset

    """
    return arg * (2 if _PY_VERSION >= (3, 10) else 1)


def _bytecode_size_to_abs_arg(
    op_idx: int,
    patch_size: int,
) -> typing.List[int]:
    """
    Transform a bytecode index offset into an instruction offset. This is necessary because the size
    of bytecode offsets changed in Python 3.10 from one byte to two bytes.

    Args:
        arg: Bytecode index offset argument.

    Returns: Bytecode instruction offset

    """
    return [op_idx + (patch_size // (2 if _PY_VERSION >= (3, 10) else 1))]


def _bytecode_size_to_rel_arg(op_delta: int, patch_size: int) -> typing.List[int]:
    """
    Transform a bytecode index offset into an instruction offset. This is necessary because the size
    of bytecode offsets changed in Python 3.10 from one byte to two bytes.

    Args:
        arg: Bytecode index offset argument.

    Returns: Bytecode instruction offset

    """
    return [op_delta + (patch_size // (2 if _PY_VERSION >= (3, 10) else 1))]


def _update_bytecode_offsets(  # noqa: C901
    bytecode: bytes,
    patch_start: int,
    patch_size: int,
    *,
    consts_offset: typing.Optional[int] = None,
) -> bytes:
    """
    Update bytecode instructions offset when necessary.

    Args:
        bytecode: Bytecode to be change
        patch_start: Index in bytecode array in which the patch bytecode starts
        patch_size: Size of the patch bytecode

    Returns: Bytecode with instruction arguments fixed

    """
    op_list = list(bytecode)
    idx = 0
    limit = len(bytecode)

    while idx < limit:
        # just skip the patch bytecode
        if idx == patch_start:
            idx += abs(patch_size)
            continue

        op = op_list[idx]

        if op in dis.hasjabs:
            # for absolute jump instructions, the argument represents the index
            op_idx = op_list[idx + 1]

            # check if the target index is after the patch
            # if so, it will require to be updated with the new size of the patch
            #
            if _bytecode_instr_abs_arg_to_idx(op_idx) > patch_start:
                op_list[idx + 1 : idx + 2] = _bytecode_size_to_abs_arg(
                    op_idx, patch_size
                )

        if op in dis.hasjrel:
            # for relative jump instructions, the argument represents a delta
            op_jump = op_list[idx + 1]

            # check if the target index is after the patch
            # if so, it will require to be updated with the new size of the patch
            if idx < patch_start < (idx + 2 + _bytecode_instr_rel_arg_to_idx(op_jump)):
                op_list[idx + 1 : idx + 2] = _bytecode_size_to_rel_arg(
                    op_jump, patch_size
                )

        if consts_offset and op == dis.opmap["LOAD_CONST"]:
            arg = op_list[idx + 1]
            if arg > consts_offset:
                op_list[idx + 1] -= consts_offset

        idx += 2

    return bytes(op_list)


def _default_patch_bytecode_factory(
    consts_idx: int, handler: typing.Optional[_FrameHandlerType]
) -> _PatchBytecodeDataType:
    """

    Callable[[int, Callable[[FrameType], None]], Tuple[bytes, Tuple[Any, ...]]]
    Callable[[int, Optional[Callable[[FrameType], None]]], Tuple[bytes, Tuple[Any, ...]]]

    Generate patch bytecode that calls a function with the current frame. `consts_idx` is the index
    at which the constants will be stored after the marker.

    Args:
        consts_idx: Index at which constants will be store in `co_consts` attribute in th ecode object.
        handler: Function handling the stack frame.

    Returns: Tuple of patch byte code and constants to add to `co_consts` attribute.

    """

    consts = (handler, _getframe, 0)

    return (
        bytes(
            [
                dis.opmap["LOAD_CONST"],
                # handlers index
                consts_idx,
                dis.opmap["LOAD_CONST"],
                # _getframe index
                consts_idx + 1,
                dis.opmap["LOAD_CONST"],
                # `0` index
                consts_idx + 2,
                dis.opmap["CALL_FUNCTION"],
                1,
                dis.opmap["CALL_FUNCTION"],
                1,
                dis.opmap["POP_TOP"],
                0,
            ]
        ),
        consts,
    )


def _inject_opcode(code_object, call_code, patch_start):
    patch_len = len(call_code)
    return _update_bytecode_offsets(
        code_object.co_code[:patch_start]
        + call_code
        + code_object.co_code[patch_start:],
        patch_start,
        patch_len,
    )


def patch_code(
    code_object: types.CodeType,
    line: int,
    handler: _FrameHandlerType,
    *,
    bytecode_factory: _PatchBytecodeFactory = _default_patch_bytecode_factory,
) -> str:
    consts_idx = len(code_object.co_consts)
    marker = str(uuid.uuid4())
    call_code, new_consts = bytecode_factory(consts_idx + 1, handler)
    co_consts = (*code_object.co_consts, marker, *new_consts)
    consts_size = len(new_consts) + 1

    patch_start, line_map_idx = _code.get_line_op_index(code_object, line)

    new_bytecode = _inject_opcode(code_object, call_code, patch_start)

    new_lnotab = new_linetable = None
    if hasattr(code_object, "co_linetable"):
        # https://github.com/python/cpython/blob/3.10/Objects/lnotab_notes.txt
        new_linetable = _gen_linetable_offset(
            code_object.co_linetable, line_map_idx, len(call_code)
        )
    else:
        new_lnotab = _gen_lnotab_offset(
            code_object.co_lnotab, patch_start, len(call_code)
        )

    _native.update_code(
        code_object,
        new_bytecode,
        co_consts,
        code_object.co_stacksize + consts_size,
        new_lnotab,
        new_linetable,
    )

    return marker


def unpatch_code(
    code_object: types.CodeType,
    marker: str,
    *,
    bytecode_factory: _PatchBytecodeFactory = _default_patch_bytecode_factory,
):
    """ """

    # find the position of the marker in the constants
    consts_idx = next(
        idx for idx, const in enumerate(code_object.co_consts) if const == marker
    )

    # regenerate patch code
    call_code, new_consts = bytecode_factory(consts_idx + 1, None)

    patch_consts_size = len(new_consts) + 1

    co_consts = (
        *code_object.co_consts[:consts_idx],
        *code_object.co_consts[consts_idx + patch_consts_size + 1 :],
    )

    patch_start = code_object.co_code.find(call_code)
    patch_len = len(call_code)

    # remove patch code from the bytecode
    # code replacement needs to happen after fixing the offsets
    new_bytecode = _update_bytecode_offsets(
        code_object.co_code,
        patch_start,
        -patch_len,
        consts_offset=consts_idx,
    ).replace(call_code, b"")

    new_lnotab = new_linetable = None
    if hasattr(code_object, "co_linetable"):
        # https://github.com/python/cpython/blob/3.10/Objects/lnotab_notes.txt
        line_map_idx = _get_bytecode_linetable_idx(code_object, patch_start)

        new_linetable = _gen_linetable_offset(
            code_object.co_linetable, line_map_idx, -len(call_code)
        )
    else:
        new_lnotab = _gen_lnotab_offset(
            code_object.co_lnotab, patch_start, -len(call_code)
        )

    _native.update_code(
        code_object,
        new_bytecode,
        co_consts,
        code_object.co_stacksize - patch_consts_size,
        new_lnotab,
        new_linetable,
    )
