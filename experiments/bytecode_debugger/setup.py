# python-live-debugger
# Copyright (C) 2022 Yunier Rojas García
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from setuptools import setup

packages = [
    "bytecode_debugger",
]

package_data = {"": ["*"], "bytecode_debugger": ["_native/*"]}

setup_kwargs = {
    "name": "bytecode-live-debugger",
    "version": "0.1.0",
    "description": "Bytecode Live debugger",
    "long_description": "None",
    "author": "Yunier Rojas García",
    "author_email": "yunier.rojas@gmail.com",
    "maintainer": "Yunier Rojas García",
    "maintainer_email": "yunier.rojas@gmail.com",
    "url": "None",
    "packages": packages,
    "package_data": package_data,
    "python_requires": ">=3.8,<3.11",
}

from build import *

build(setup_kwargs)

setup(**setup_kwargs)
