# python-live-debugger
# Copyright (C) 2022 Yunier Rojas García
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


__all__ = ["get_code", "get_line_op_index"]

import ctypes
import dis
import importlib
import inspect
import itertools
import types
import typing
from os import path


def _import_file(fn: str) -> types.ModuleType:
    """

    Args:
        fn: Relative path to the system path that can be imported

    Returns:

    """
    mod, ext = path.splitext(fn)
    return importlib.import_module(mod.replace(path.sep, "."))


def _has_code(obj: typing.Any) -> bool:
    if inspect.isclass(obj):
        return True

    try:
        dis.code_info(obj)
        return True
    except (TypeError, SyntaxError):
        return False


def _get_object_file(obj: typing.Any) -> typing.Optional[str]:
    try:
        return inspect.getfile(obj)
    except TypeError:
        return None


def _get_module_codes(module: types.ModuleType) -> typing.List[types.CodeType]:
    seen = {module}
    codes = set()
    mod_file = inspect.getfile(module)
    members = [new for _, new in inspect.getmembers(module, _has_code)]
    while members:
        obj = members.pop()

        if obj in seen:
            continue

        seen.add(obj)

        obj_file = _get_object_file(obj)
        if obj_file != mod_file:
            continue

        members.extend([obj for _, obj in inspect.getmembers(obj, _has_code)])

        if isinstance(obj, types.CodeType):
            codes.add(obj)
            members.extend(obj.co_consts)

    return sorted(codes, key=lambda x: x.co_firstlineno)


def _iter_lines_lnotab(
    code: types.CodeType,
) -> typing.Generator[typing.Tuple[int, int, int], None, None]:
    if not hasattr(code, "co_linetable"):
        byte_offsets = list(itertools.accumulate([int(i) for i in code.co_lnotab[::2]]))
        lines = [
            code.co_firstlineno + inc
            for inc in itertools.accumulate([int(i) for i in code.co_lnotab[1::2]])
        ]
        yield from zip(byte_offsets, lines, map(lambda x: x * 2, itertools.count()))


def _iter_lines_linetable(
    code: types.CodeType,
) -> typing.Generator[typing.Tuple[int, int, int], None, None]:
    line = code.co_firstlineno
    end = 0
    co_linetable = [ctypes.c_int8(byte).value for byte in code.co_linetable]  # type: ignore[attr-defined]
    table_iter = iter(co_linetable)
    for idx, (sdelta, ldelta) in enumerate(zip(table_iter, table_iter)):
        if ldelta == 0:  # No change to line number, just accumulate changes to end
            end += sdelta
            continue
        start = end
        end = start + sdelta
        if ldelta == -128:  # No valid line number -- skip entry
            continue
        line += ldelta
        if end == start:  # Empty range, omit.
            continue
        yield start, line, idx * 2


def get_line_op_index(code, line):
    source = (
        _iter_lines_linetable if hasattr(code, "co_linetable") else _iter_lines_lnotab
    )
    for op, ln, idx in source(code):
        if ln == line:
            return op, idx


def get_code(fn: str, line: int) -> types.CodeType:
    mod = _import_file(fn)
    codes = _get_module_codes(mod)

    for ref in codes:
        match = get_line_op_index(ref, line)
        if match:
            return ref

    raise ValueError(f"Invalid line number: {line}")
