# python-live-debugger
# Copyright (C) 2022 Yunier Rojas García
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import glob
from distutils.command.build_ext import build_ext
from distutils.core import Extension

from Cython.Build import cythonize
from setuptools.config.expand import find_packages

cython_compile_args = [
    "-std=c18",
    "-g0",
    "-O3",
    "-Werror",
    "-Wno-deprecated-declarations",
    "-Wno-unreachable-code-fallthrough",
    "-Wno-nullability-completeness",
    "-Wno-expansion-to-defined",
    "-Wno-unused-variable",
]

c_compile_args = [
    "-std=c18",
    "-g0",
    "-O3",
    "-Werror",
]


def build(setup_kwargs):
    extensions = [
        Extension(
            "*",
            sources=["*.pyx"],
            extra_compile_args=cython_compile_args,
        ),
        Extension(
            "bytecode_debugger._native",
            sources=glob.glob("_native/*.c"),
            extra_compile_args=c_compile_args,
        ),
    ]
    ext_modules = cythonize(
        extensions,
        compiler_directives={"binding": True, "language_level": "3str"},
    )

    setup_kwargs.update(
        {
            "ext_modules": ext_modules,
            "cmdclass": {"build_ext": build_ext},
            "package_dir": {"": ".."},
            "packages": find_packages(where=".."),
        }
    )
