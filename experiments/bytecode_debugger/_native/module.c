/**
python-live-debugger
Copyright (C) 2022 Yunier Rojas García

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
**/

#define PY_SSIZE_T_CLEAN
#include <Python.h>

#include "modifier.h"

char update_code_func_docs[] =
    "Modify key attributes in a code object.\n\
                                                                       \n\
    Args:                                                              \n\
        code_object (types.CodeType): Code object to update            \n\
        co_code (typing.Optional[bytes]): New bytecode to be set       \n\
        co_consts (typing.Optional[tuple[typing.Any, ...]]): New constants to be set \n\
        co_stacksize (typing.Optional[int]): New stack size to be set  \n\
        co_lnotab (typing.Optional[bytes]): New line table to be set   \n\
        co_linetable (typing.Optional[bytes]): New line table to be set\n\
";

PyMethodDef native_funcs[] = {{"update_code", (PyCFunction)update_code,
                               METH_VARARGS, update_code_func_docs},
                              {NULL}};

char native_mod_docs[] =
    "C utilities for the live debugger.\n\
This module only responsibility is to update function code object attributes, in special bytecode.\n\
Changing a code object in Python is impossible as the object is immutable, thus the purpose of this extension.\n\
";

PyModuleDef native_mod = {PyModuleDef_HEAD_INIT,
                          "_native",
                          native_mod_docs,
                          -1,
                          native_funcs,
                          NULL,
                          NULL,
                          NULL,
                          NULL};

PyMODINIT_FUNC PyInit__native(void) { return PyModule_Create(&native_mod); }
