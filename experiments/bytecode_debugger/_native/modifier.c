/**
python-live-debugger
Copyright (C) 2022 Yunier Rojas García

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
**/

#include "modifier.h"

#include <Python.h>
#include <code.h>

PyObject *update_code(PyObject *self, PyObject *py_args) {
  PyCodeObject *code_object = nullptr;

  PyObject *old_code = Py_None;
  PyObject *old_consts = Py_None;
  PyObject *old_lnotab = Py_None;
  PyObject *old_linetable = Py_None;
  int old_stacksize = -1;

  PyObject *co_code = Py_None;
  PyObject *co_consts = Py_None;
  PyObject *co_lnotab = Py_None;
  PyObject *co_linetable = Py_None;
  int co_stacksize = -1;

  if (!PyArg_ParseTuple(py_args, "OOOiOO", &code_object, &co_code, &co_consts,
                        &co_stacksize, &co_lnotab, &co_linetable)) {
    return NULL;
  }

  if ((code_object == nullptr) || !PyCode_Check(code_object)) {
    PyErr_SetString(PyExc_TypeError, "invalid code_object argument");
    return NULL;
  }

  if ((co_code != Py_None)) {
    old_code = code_object->co_code;
    Py_XINCREF(co_code);
    code_object->co_code = co_code;
  }

  if ((co_consts != Py_None)) {
    old_consts = code_object->co_consts;
    Py_XINCREF(co_consts);
    code_object->co_consts = co_consts;
  }

  if ((co_stacksize >= 0)) {
    old_stacksize = code_object->co_stacksize;
    code_object->co_stacksize = co_stacksize;
  }

#if PY_VERSION_HEX < 0x030a00f0
  if ((co_lnotab != Py_None)) {
    Py_XINCREF(co_lnotab);
    old_lnotab = code_object->co_lnotab;
    code_object->co_lnotab = co_lnotab;
  }
#else
  if ((co_linetable != Py_None)) {
    // https://peps.python.org/pep-0626/
    // https://github.com/python/cpython/blob/3.10/Objects/lnotab_notes.txt
    Py_XINCREF(co_linetable);
    old_linetable = code_object->co_linetable;
    code_object->co_linetable = co_linetable;
  }
#endif

  return PyTuple_Pack(5, old_code, old_consts,
                      Py_BuildValue("i", old_stacksize), old_lnotab,
                      old_linetable);
}
