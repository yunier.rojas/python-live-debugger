# Changelog


<!--next-version-placeholder-->

## v0.1.0 (2023-01-15)
### Feature
* Fix release job ([`f9e9257`](https://gitlab.com/yunier.rojas/python-live-debugger/-/commit/f9e9257a0347ad125932835759430bf8f2428fc3))
* Fix release job ([`f808daf`](https://gitlab.com/yunier.rojas/python-live-debugger/-/commit/f808daf74126e1014dc51e9173bb176c2207d98e))
* Fix release job ([`d403df3`](https://gitlab.com/yunier.rojas/python-live-debugger/-/commit/d403df30fc56116b6560298c3c11b9a462062602))
* Set right version ([`1f5f57e`](https://gitlab.com/yunier.rojas/python-live-debugger/-/commit/1f5f57e50be7131d93f2689a8bff1f10665a2c17))
* Live debugger library ([`feafb52`](https://gitlab.com/yunier.rojas/python-live-debugger/-/commit/feafb52d40d3aeca9d3e880e9bd9b0adde3dfaba))
