.. Python Live Debugger documentation master file, created by
   sphinx-quickstart on Fri Dec 16 22:55:58 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Live Debugger
=============

.. image:: https://readthedocs.org/projects/python-live-debugger/badge/?version=latest
    :target: https://python-live-debugger.readthedocs.io/en/latest/?badge=latest
    :alt: Documentation Status

.. image:: _static/docstr_coverage_badge.svg
    :target: docstr_coverage_badge.svg
    :alt: Docs coverage

Introducing Live Debugger, a simple library for CPython that allows you to collect stacktrace data from
running processes in production environments without stopping the execution of the process. With Live Debugger,
you can quickly and easily identify and fix bugs in production environments, improve the performance of your
processes, and gain valuable insights into how your code is running in the wild.

Built with safety in mind, Live Debugger provides safe building blocks and concepts for developers to collect
stack snapshots from processes. It is designed to be minimal and extensible, with no assumptions made on what
the process plans to do with the collected data, focusing only on providing immutable and safe data.

Whether you're a seasoned developer or just getting started, Live Debugger makes it easy to debug CPython
processes in production. Try it out today and see the difference it can make in your workflow.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   readme
   adrs/index
   code/modules



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
