.. include:: ../helpers/.tags.rst


Live debugger library
=====================


Context
*******
Live debugging is sometimes the fastest way to get insights into production issues. There are some tools available
such as Rookout, Ozcode, and Google Python Cloud Debugger (CDBG). They are either not open source or deprecated. In the
case of CDBG, the library is completely oriented to Google Cloud infrastructure making it really hard to detach and
maintain. For the context of this decision, proprietary software was left out.


Problem Statement
*****************
I want to use a python library in a private project to safety collect insights of issues in production.

Decision Drivers
****************

1. Free (as in freedom) or open source
3. Safe
2. Lean
4. Fast

Considered Options
******************

CDBG
----

`Python Cloud Debugger <https://github.com/GoogleCloudPlatform/cloud-debug-python>`_ is a library created by
Google to enable live debugger sessions on Google Cloud Platform to collect on demand information about running
python processes.

* :blue:`good` License: Apache 2.0 License
* :blue:`good` Fast: C++ code handles all execution of callbacks
* :red:`bad` Lean: Hard to use without the Google mumble-jumble
* :red:`bad` Safe: Google product will be `deprecated soon <https://cloud.google.com/debugger/docs/setup/python>`_


DIY
---
Start a Python library that allows runtime collection of running processes data as lean as possible. Focus only on
the debugging process and delegate other architectural decisions to the future users of the library.

* :blue:`good` License: I can choose it
* :blue:`good` Small: Keep the library as minimal as possible
* :blue:`good` Safety: Bring safety by keeping original code structure
* :red:`bad` Time: Coding such a library requires time to develop and maintain since it depends on Python internals


Decision Outcome
****************

I have decide to implement a Python library from scratch to match all my requirements. This is not an easy decision
as it requires a LOT of time to develop and maintain but I think by having a open source project, can allow other
developers to contribute and expand the library.

