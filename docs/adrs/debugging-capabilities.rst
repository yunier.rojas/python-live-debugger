.. include:: ../helpers/.tags.rst


Debugging processes
===================

Context
*******
OK, so let's create a debugging library to debug running processes in production.

Problem Statement
*****************
Problem is, how to "debug" a production Python process?

Decision Drivers
****************

3. Safe
2. Lean
4. Fast

Considered Options
******************

``sys.settrace``
----------------

`sys.settrace <https://docs.python.org/3/library/sys.html#sys.settrace>`_ is CPython function used for debugger,
and profilers to install specific functions that will receive code execution information from the CPython
interpreter.

* :blue:`good` Lean: It is just setting a function and it works
* :blue:`good` Safe: Since it comes in the standard library, you can say it can not get safer than that
* :blue:`good` Safe: ``sys.settrace`` effect is thread-safe, meaning each thread debugging session is independent
* :red:`bad` Fast: Once you install the function, it gets executed for every function call, return or exception


Bytecode
--------
Google Python Cloud Debugger relies on CPython bytecode to debug running processes. In oversimplified nutshell,
to debug a function, the library changes the python bytecode of the function to trigger a function that make the
frame immutable and pass it to all registered callbacks.


* :blue:`bad` Lean: Definitely... not. Code objects are immutable in CPython by default, so some hacks will be necessary
* :blue:`bad` Safe: Python bytecode changes very often from version to version
* :blue:`bad` Safe: Errors patching the bytecode end up in data corruption
* :red:`good` Fast: Code will only get executed when the target bytecode is executed


Decision Outcome
****************
Main debugger will use ``sys.settrace`` by default. This bring some extra benefits and limitations of course but I
believe safety about everything else matter the most for debugging processes running in production. As outcome for
this problem, the library should allow to be easily extended for other users looking for more performant solutions.

