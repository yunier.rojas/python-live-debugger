import sys
import typing

import pytest

from tests import mod_for_test


def test_tracing_debugger_with_deactivate_should_remove_trace_function(
    tracing_debugger,
):
    assert sys.gettrace() != tracing_debugger._trace_dispatch
    tracing_debugger.activate()
    assert sys.gettrace() == tracing_debugger._trace_dispatch
    tracing_debugger.deactivate()
    assert sys.gettrace() != tracing_debugger._trace_dispatch


@pytest.mark.parametrize(
    "line,activate",
    mod_for_test.DATA,
)
def test_tracing_debugger_with_patch_revert_should_remove_trace_function(
    line: int, activate: typing.Callable[[], int], tracing_debugger
):
    assert sys.gettrace() != tracing_debugger._trace_dispatch
    tracing_debugger.activate()
    assert sys.gettrace() == tracing_debugger._trace_dispatch

    cookie = tracing_debugger.add_point("tests/mod_for_test.py", line, print)

    tracing_debugger.clear_point(cookie)

    assert sys.gettrace() != tracing_debugger._trace_dispatch


@pytest.mark.parametrize(
    "line,activate",
    mod_for_test.DATA,
)
def test_tracing_debugger_with_deactivated_should_not_trigger_callback(
    line: int, activate: typing.Callable[[], int], tracing_debugger
):
    assert sys.gettrace() != tracing_debugger._trace_dispatch

    info = []

    def callback(data):
        info.append(data)

    tracing_debugger.add_point("tests/mod_for_test.py", line, callback)

    activate()

    assert len(info) == 0


@pytest.mark.parametrize(
    "line,activate",
    mod_for_test.DATA,
)
def test_tracing_debugger_with_late_deactivated_should_not_trigger_callback(
    line: int, activate: typing.Callable[[], int], tracing_debugger
):
    assert sys.gettrace() != tracing_debugger._trace_dispatch

    tracing_debugger.activate()

    info = []

    def callback(data):
        info.append(data)

    tracing_debugger.add_point("tests/mod_for_test.py", line, callback)

    tracing_debugger.deactivate()

    activate()

    assert len(info) == 0


@pytest.mark.parametrize(
    "line,activate",
    mod_for_test.DATA,
)
def test_tracing_debugger_with_late_activation_should_trigger_callback(
    line: int, activate: typing.Callable[[], int], tracing_debugger
):
    assert sys.gettrace() != tracing_debugger._trace_dispatch

    info = []

    def callback(data):
        info.append(data)

    tracing_debugger.add_point("tests/mod_for_test.py", line, callback)
    tracing_debugger.activate()

    activate()

    assert len(info) == 1


@pytest.mark.debugger_benchmark
@pytest.mark.parametrize(
    "line,activate",
    mod_for_test.DATA,
)
def test_debugger_with_callback_should_be_fast(
    line, activate, tracing_debugger, benchmark
):
    tracing_debugger.activate()
    tracing_debugger.add_point("tests/mod_for_test.py", line, lambda x: None)
    benchmark(activate)
