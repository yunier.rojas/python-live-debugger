import pytest


@pytest.fixture(autouse=True)
def reimport_mod_for_test():
    import importlib

    from tests import mod_for_test

    importlib.reload(mod_for_test)


@pytest.fixture(autouse=True)
def tracing_debugger():
    from live_debugger import api, debugger

    obj = debugger.LiveDebugger()
    api.use(obj)
    yield obj
    obj.deactivate()
