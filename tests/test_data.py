import io
import itertools
from sys import _getframe

import pytest

from live_debugger import data, typing


class CustomDict(dict):
    pass


recursive_obj = [
    1,
]
recursive_obj.append(recursive_obj)

stream = io.BytesIO(b"")


@pytest.mark.parametrize(
    "obj,expected",
    [
        (
            CustomDict(a=1),
            typing.Value(
                type="tests.test_data.CustomDict",
                value=(("a", typing.Value(type="int", value="1")),),
            ),
        ),
        (
            [1, 2, 3],
            typing.Value(
                type="list",
                value=(
                    typing.Value(type="int", value="1"),
                    typing.Value(type="int", value="2"),
                    typing.Value(type="int", value="3"),
                ),
            ),
        ),
        (
            stream,
            typing.Value(
                type="_io.BytesIO", value=f"<_io.BytesIO object at {hex(id(stream))}>"
            ),
        ),
        (
            recursive_obj,
            typing.Value(
                type="list",
                value=(
                    typing.Value(type="int", value="1"),
                    typing.Value(
                        type="list",
                        value=(
                            typing.Value(type="int", value="1"),
                            typing.Value(
                                type="list",
                                value=(
                                    typing.Value(type="int", value="1"),
                                    typing.Value(type="list", value="[1, [...]]"),
                                ),
                            ),
                        ),
                    ),
                ),
            ),
        ),
        (itertools.count(), typing.Value(type="itertools.count", value="count(0)")),
        (range(10), typing.Value(type="range", value="range(0, 10)")),
    ],
)
def test_transform_object_with_object_should_return_expected(obj, expected):
    trans = data._transform_object(obj, 2)
    assert trans == expected, trans


numbers = list(range(10))


@pytest.mark.parametrize(
    "obj,activate,expected",
    [
        (itertools.count(), lambda x: next(x), 0),
        ((i for i in range(10)), list, numbers),
        (range(10), list, numbers),
    ],
)
def test_transform_object_with_special_object_should_not_mutate_objects(
    obj, activate, expected
):
    _ = data._transform_object(obj, 2)
    result = activate(obj)
    assert result == expected, result


@pytest.mark.data_benchmark
def test_transform_object_with_frame_should_transform_into_model(benchmark):
    frame = _getframe(0)
    res = benchmark(data.transform_frame, frame)
    assert isinstance(res, list)
    assert isinstance(res[0], typing.FrameData)
