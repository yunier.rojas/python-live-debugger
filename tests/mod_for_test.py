import contextlib
import sys


class ClassA:
    def method(self):
        return 1


def funct():
    return 1


def gen_funct():
    yield 1


# fmt: off
lambda_var = lambda: (  # noqa: E731
    1
)
# fmt: on

with contextlib.suppress(ValueError):
    raise ValueError

for var in range(10):
    str(var)


def nested1():
    def nested2():
        def nested3():
            return 1

        return nested3

    return nested2


def nested_class():
    class B:
        class C:
            def nested4(self):
                def nested5():
                    return 1

                return nested5

    return B


def for_function():
    for c in range(10, 0, -1):
        if c == 1:
            return c


def try_function():
    try:
        1 / 0
    except ZeroDivisionError:
        return 1


def finally_function():
    try:
        1 / 0
    except ZeroDivisionError:
        pass
    finally:
        return 1


METHOD_LINE = 7
FUNCTION_LINE = 11
GEN_LINE = 15
LAMBDA_LINE = 20
NESTED1_LINE = 34
NESTED2_LINE = 46
FOR_LINE1 = 56
TRY_LINE1 = 61
TRY_LINE2 = 63
FINALLY_LINE1 = 68
FINALLY_LINE2 = 70
FINALLY_LINE3 = 72

DATA = [
    # line no, activator
    (METHOD_LINE, lambda: ClassA().method()),
    (FUNCTION_LINE, lambda: funct()),
    (GEN_LINE, lambda: next(gen_funct())),
    (LAMBDA_LINE, lambda: lambda_var()),
    (NESTED1_LINE, lambda: nested1()()()),
    (NESTED2_LINE, lambda: nested_class()().C().nested4()()),
    (FOR_LINE1, lambda: for_function()),
    (TRY_LINE1, lambda: try_function()),
    (TRY_LINE2, lambda: try_function()),
    (FINALLY_LINE1, lambda: finally_function()),
    (FINALLY_LINE2, lambda: finally_function()),
]

if sys.version_info[:2] not in [(3, 10)]:
    # python 3.10 handles finally code differently of any other version
    # skipping this case for now
    DATA.extend(
        [
            (FINALLY_LINE3, lambda: finally_function()),
        ]
    )
