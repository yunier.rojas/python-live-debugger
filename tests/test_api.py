import typing

import pytest

from live_debugger import api
from live_debugger import typing as ld
from tests import mod_for_test


@pytest.mark.parametrize(
    "line,activate",
    mod_for_test.DATA,
)
def test_add_point_with_valid_data_should_successfully_install_callback(line, activate):
    info = []

    api.activate()

    def callback(data: typing.List[ld.FrameData]):
        info.append(data)

    api.add_point("tests/mod_for_test.py", line, callback)

    activate()

    assert len(info) == 1


@pytest.mark.parametrize(
    "line,activate",
    mod_for_test.DATA,
)
def test_clear_point_with_cookie_should_remove_callback(line, activate):
    info = []

    api.activate()

    def callback(data: typing.List[ld.FrameData]):
        info.append(data)

    cookie = api.add_point("tests/mod_for_test.py", line, callback)
    api.clear_point(cookie)

    activate()

    assert len(info) == 0
