from flask import Flask, jsonify

from live_debugger import api


class LogCollector:
    logs = []
    cookie = None

    @classmethod
    def install(cls):
        cls.cookie = api.add_point(
            "examples/flask_example/app.py", 34, callback=cls.collect
        )

    @classmethod
    def uninstall(cls):
        api.clear_point(cls.cookie)

    @classmethod
    def collect(cls, data):
        cls.logs.append(data)


def create_app(test_config=None):
    LogCollector.install()
    app = Flask(__name__)
    api.activate()

    @app.route("/test", methods=["GET"])
    def test():
        from flask import request

        values = request.values  # noqa: F841
        return "ok"

    @app.route("/logs", methods=["GET"])
    def logs():
        return jsonify(LogCollector.logs)

    return app
