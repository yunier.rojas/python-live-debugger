.PHONY: prepare test build docs-api docs release


prepare:
	poetry run black live_debugger experiments examples tests
	poetry run black experiments/*/*.pyx
	poetry run isort live_debugger experiments examples tests
	clang-format --style=Google -i experiments/bytecode_debugger/_native/*


test:
	poetry run black --check live_debugger experiments examples tests
	poetry run isort --check live_debugger experiments examples tests
	poetry run flake8 live_debugger examples tests
	poetry run pytest -m 'not data_benchmark and not debugger_benchmark' --cov=live_debugger --cov-config=.coveragerc --cov-report=xml
	poetry run docstr-coverage -C .docstr.yml live_debugger
	poetry run safety check
	poetry run bandit -r live_debugger
	poetry run mypy live_debugger

benchmark-data:
	poetry run pytest -m 'data_benchmark' --benchmark-compare --benchmark-autosave

benchmark-debugger:
	poetry run pytest -m 'debugger_benchmark' --benchmark-compare --benchmark-autosave


build:
	poetry build --no-interaction



docs-api:
	rm -rf docs/code
	sphinx-apidoc -o docs/code --separate --module-first live_debugger


docs:
	$(MAKE) -C docs html


release:
	poetry run semantic-release publish
